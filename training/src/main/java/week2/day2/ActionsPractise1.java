package week2.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class ActionsPractise1 {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://jqueryui.com/sortable/");
		driver.switchTo().frame(0);
		
		WebElement source = driver.findElementByXPath("//ul[@id='sortable']/li");
		
		WebElement target = driver.findElementByXPath("//ul[@id='sortable']/li[4]");
		//Point location = target.getLocation();
		
		Actions builder=new Actions(driver);
		builder.dragAndDropBy(source, target.getLocation().getX(), target.getLocation().getY()).perform();
		

	}

}
