package week2.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class ActionsPractise {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://jqueryui.com/draggable/");
		
		driver.switchTo().frame(0);
		WebElement draggable = driver.findElementById("draggable");
		
		Actions builder=new Actions(driver);
		
		
		builder.dragAndDropBy(draggable, draggable.getLocation().getX()+100, draggable.getLocation().getY()+100).perform();
		

	}

}
