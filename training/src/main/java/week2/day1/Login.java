package week2.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("TestLeaf");
		driver.findElementById("createLeadForm_firstName").sendKeys("Hari Prasad");
		driver.findElementById("createLeadForm_lastName").sendKeys("Radhakrishnan");
		
		/*WebElement submit = driver.findElementByClassName("decorativeSubmit");
		System.out.println(submit.getSize());
		System.out.println(submit.getLocation());*/
		/*driver.findElementByName("submitButton").click();
		String title = driver.getTitle();
		
		System.out.println(title);
		
		driver.close();*/
		WebElement source = driver.findElementById("createLeadForm_dataSourceId");
		Select dropDown1=new Select (source);
		dropDown1.selectByVisibleText("Existing Customer");
		
		WebElement marketingCampaign = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dropDown2=new Select (marketingCampaign);
		List<WebElement> options = dropDown2.getOptions();
		int size = options.size();
		dropDown2.selectByIndex(size-1);
		

	}

}
