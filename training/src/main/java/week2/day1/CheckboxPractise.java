package week2.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class CheckboxPractise {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/checkbox.html");
		
		driver.findElementByXPath("(//div[@class='example'])[1]/input[1]").click();
		driver.findElementByXPath("(//div[@class='example'])[1]/input[3]").click();
		driver.findElementByXPath("(//div[@class='example'])[1]/input[4]").click();
		
		boolean selected = driver.findElementByXPath("(//div[@class='example'])[2]/input").isSelected();
		System.out.println("Is checkbox selected: "+selected);
		
		boolean deSelect1 = driver.findElementByXPath("(//div[@class='example'])[3]/input[1]").isSelected();
		if(deSelect1==true){
			driver.findElementByXPath("(//div[@class='example'])[3]/input[1]").click();
		}
		
		boolean deSelect2 = driver.findElementByXPath("(//div[@class='example'])[3]/input[2]").isSelected();
		if(deSelect2==true){
			driver.findElementByXPath("(//div[@class='example'])[3]/input[2]").click();
		}
		
		driver.findElementByXPath("(//div[@class='example'])[4]/input[1]").click();
		driver.findElementByXPath("(//div[@class='example'])[4]/input[2]").click();
		driver.findElementByXPath("(//div[@class='example'])[4]/input[3]").click();
		driver.findElementByXPath("(//div[@class='example'])[4]/input[4]").click();
		driver.findElementByXPath("(//div[@class='example'])[4]/input[5]").click();
		driver.findElementByXPath("(//div[@class='example'])[4]/input[6]").click();
		
	}

}
