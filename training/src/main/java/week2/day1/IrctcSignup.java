package week2.day1;



import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IrctcSignup {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		
		WebElement country = driver.findElementById("userRegistrationForm:countries");
		Select ddCountry=new Select(country);
		List<WebElement> options = ddCountry.getOptions();
		List<String> eCountries=new ArrayList<String>();
		
		//Getting all the Countries starting with E in a collection
		for (WebElement countryList : options) {
			String countries = countryList.getText();
			if(countries.startsWith("E")){
				eCountries.add(countries);
			}
			
		}
		
		//To select the second value starts with E
		for (WebElement list : options) {
			String countries = list.getText();
			if(countries.equals(eCountries.get(1))){
				list.click();
			}
		}
		
	}

}	
		/*driver.findElementById("userRegistrationForm:userName").sendKeys("hari789");
		driver.findElementById("userRegistrationForm:password").sendKeys("testPassWord");
		
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("testPassWord");
		
		WebElement hintQues = driver.findElementById("userRegistrationForm:securityQ");
		Select ddHint=new Select(hintQues);
		ddHint.selectByValue("1");
		
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Rajavidyalayam");
		
		WebElement prefLang = driver.findElementById("userRegistrationForm:prelan");
		Select ddPrefLang=new Select(prefLang);
		ddPrefLang.selectByVisibleText("English");
		
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Hari Prasad");
		driver.findElementById("userRegistrationForm:gender:0").click();
		driver.findElementById("userRegistrationForm:maritalStatus:0").click();
		
		
		WebElement day = driver.findElementById("userRegistrationForm:dobDay");
		Select ddDay=new Select(day);
		ddDay.selectByVisibleText("26");
		
		
		WebElement month = driver.findElementById("userRegistrationForm:dobMonth");
		Select ddmonth=new Select(month);
		ddmonth.selectByVisibleText("FEB");
		
		WebElement year = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select ddYear=new Select(year);
		ddYear.selectByVisibleText("1979");
		
		WebElement occupation = driver.findElementById("userRegistrationForm:occupation");
		Select ddOccu=new Select(occupation);
		ddOccu.selectByVisibleText("Professional");
		
		WebElement country = driver.findElementById("userRegistrationForm:countries");
		Select ddCountry=new Select(country);
		ddCountry.selectByVisibleText("India");
		
		driver.findElementById("userRegistrationForm:email").sendKeys("r.hariprasad@gmail.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("8884151543");
		
		WebElement nationality = driver.findElementById("userRegistrationForm:nationalityId");
		Select ddNation=new Select(nationality);
		ddNation.selectByVisibleText("India");
		
		driver.findElementById("userRegistrationForm:address").sendKeys("111, Gerugambakkam");
		
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600122",Keys.TAB);
		Thread.sleep(3000);
		WebElement city = driver.findElementById("userRegistrationForm:cityName");
		Select ddCity=new Select(city);
		ddCity.selectByIndex(1);
		
		WebElement post = driver.findElementById("userRegistrationForm:postofficeName");
		Select ddPost=new Select(post);
		ddPost.selectByIndex(1);
		
		driver.findElementById("userRegistrationForm:landline").sendKeys("8884151543");
		driver.findElementByLinkText("Submit Registration Form>>>").click();
		
		
		*/


