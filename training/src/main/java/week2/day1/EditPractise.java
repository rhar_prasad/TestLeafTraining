package week2.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;

public class EditPractise {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/Edit.html");
		
		driver.findElementById("email").sendKeys("r.hariprasad@gmail.com");
		
		driver.findElementByXPath("//input[@type='text' and @value='Append ']").sendKeys("test",Keys.TAB);
		
		String textVal = driver.findElementByXPath("//input[@name='username' and @value='TestLeaf']").getAttribute("value");
		System.out.println(textVal);
		driver.findElementByXPath("//input[@name='username' and @value='Clear me!!']").clear();
		boolean enabled = driver.findElementByXPath("//*[@id='contentblock']/section/div[5]/div/div/input").isEnabled();
		System.out.println(enabled);
		
		

	}

}
