package week2.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SelectPractise {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/Dropdown.html");
		
		WebElement dd1 = driver.findElementById("dropdown1");
		Select dropDown1=new Select(dd1);
		dropDown1.selectByIndex(2);
		
		WebElement dd2 = driver.findElementByName("dropdown2");
		Select dropDown2=new Select(dd2);
		dropDown2.selectByVisibleText("Selenium");
		
		WebElement dd3 = driver.findElementById("dropdown3");
		Select dropDown3=new Select(dd3);
		dropDown3.selectByValue("3");
		
		WebElement dd4 = driver.findElementByClassName("dropdown");
		Select dropDown4=new Select(dd4);
		
		List<WebElement> options = dropDown4.getOptions();
		int size = options.size();
		System.out.println("Number of options are: "+size);
		
		driver.findElementByXPath("(//select)[5]").sendKeys("UFT/QTP");
		
		WebElement dd6 = driver.findElementByXPath("(//select)[6]");
		Select dropDown6=new Select(dd6);
		dropDown6.selectByVisibleText("Loadrunner");
		
		
		

	}

}
