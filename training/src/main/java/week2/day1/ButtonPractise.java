package week2.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.chrome.ChromeDriver;

public class ButtonPractise {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/Button.html");
		driver.findElementById("home").click();
		
		driver.findElementByXPath("//*[text()='Button']").click();
		Point location = driver.findElementById("position").getLocation();
		System.out.println("Button position is: "+location);
		
		String color = driver.findElementById("color").getCssValue("background-color");
		
		System.out.println("Button color is: "+color);
		
		Dimension size = driver.findElementById("size").getSize();
		
		System.out.println("Button size is: "+size);
		
	

	}

}
