package week2.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LinkPractise {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/Link.html");
		
		driver.findElementByLinkText("Go to Home Page").click();
		driver.findElementByXPath("//*[text()='HyperLink']").click();
		String linkToGo = driver.findElementByLinkText("Find where am supposed to go without clicking me?").getAttribute("href");
		
		System.out.println("Link will go to: "+linkToGo);
		
		driver.findElementByLinkText("Verify am I broken?").click();
		String error = driver.getTitle();
		if(error.equalsIgnoreCase("HTTP Status 404 – Not Found")){
			System.out.println("Given link is broken");
			driver.navigate().back();
		}
		else{
			System.out.println("Given link is not broken");
		}
		
		driver.findElementByXPath("(//a[text()='Go to Home Page'])[2]").click();
		driver.findElementByXPath("//*[text()='HyperLink']").click();
		
		List<WebElement> links = driver.findElementsByTagName("a");
		int linksCount = links.size();
		System.out.println("Number of links in this page is: "+linksCount);
	}

}
