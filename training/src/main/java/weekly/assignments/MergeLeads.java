package weekly.assignments;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLeads {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("//input[@id='partyIdFrom']/following::a").click();
		
		List<String> winRef=new ArrayList<String>(driver.getWindowHandles());
		//Set<String> allWinRef = driver.getWindowHandles();
		driver.switchTo().window(winRef.get(1));
		driver.findElementByName("id").sendKeys("12017");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//table[@class='x-grid3-row-table']//tr/td[1]/div/a").click();
		driver.switchTo().window(winRef.get(0));
		
		driver.findElementByXPath("//input[@id='partyIdTo']/following::a").click();
		
		List<String> winRef1=new ArrayList<String>(driver.getWindowHandles());
		//Set<String> allWinRef = driver.getWindowHandles();
		driver.switchTo().window(winRef1.get(1));
		driver.findElementByName("id").sendKeys("12018");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//table[@class='x-grid3-row-table']//tr/td[1]/div/a").click();
		
		driver.switchTo().window(winRef.get(0));
		driver.findElementByLinkText("Merge").click();
		
		driver.switchTo().alert().accept();
		
		
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByName("id").sendKeys("12017");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		String errTxt = driver.findElementByXPath("//div[text()='No records to display']").getText();
		
		if(errTxt.equals("No records to display")){
			System.out.println("Leads merged successfully");
		}
		else{
			System.out.println("Leads not merged successfully");
		}

		


	}

}
