package weekly.assignments;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ErailSort {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://erail.in/");

		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MS", Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("TPJ", Keys.TAB);

		driver.findElementById("chkSelectDateOnly").click();

		List<WebElement> trainNos = driver.findElementsByXPath("//table[@class='DataTable TrainList']//tr");

		Set<String> trainSet = new TreeSet<String>();

		for (int i = 1; i <= trainNos.size(); i++) {

			trainSet.add(
					driver.findElementByXPath("//table[@class='DataTable TrainList']//tr[" + i + "]/td[1]").getText());

		}

		driver.findElementByLinkText("Train").click();

		List<WebElement> sortedTrainNos = driver.findElementsByXPath("//table[@class='DataTable TrainList']//tr");

		List<String> sortedSet = new ArrayList<String>(trainSet);

		String match = "Sorting is verified and looks good";
		
		if (sortedTrainNos.size() == sortedSet.size()) {

			for (int i = 1; i < sortedTrainNos.size(); i++) {

				String trainNum = driver
						.findElementByXPath("//table[@class='DataTable TrainList']//tr[" + i + "]/td[1]").getText();
				if (trainNum.equals(sortedSet.get(i-1))) {
					
					System.out.println(trainNum+"  "+sortedSet.get(i-1));

				} else {
					System.out.println(trainNum+"  "+sortedSet.get(i-1));
					match = "Sorting is verified and looks not good";
					break;
				}

			}
		} else {
			System.out.println("count is not matching");
		}

		System.out.println(match);

		

	}

}
