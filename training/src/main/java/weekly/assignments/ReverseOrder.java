package weekly.assignments;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ReverseOrder {

	public static void main(String[] args) {
		
				
		List<String> compList=new ArrayList<String>();
		
		compList.add("HCL");
		compList.add("TCS");
		compList.add("Aspire Systems");
		
		Collections.sort(compList);
		Collections.reverse(compList);
		
		for (String companies : compList) {
			System.out.println(companies);
			
		}
		

	}

}
