package weekly.assignments;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class UniqueChars {

	public static void main(String[] args) {
		Map<Character,Integer> charMap=new LinkedHashMap<Character,Integer>();
		String str="PayPal India";
		
		char[] charArray = str.toCharArray();
		
		for (char ch : charArray) {
			
			if(charMap.containsKey(ch)){
				charMap.put(ch, charMap.get(ch)+1);
			}
			else{
				charMap.put(ch, 1);
			}
			
		}
		
		Set<Character> keys = charMap.keySet();
		
		for (Character character : keys) {
			if(character!=' '&& charMap.get(character)==1)
			System.out.print(character);
			
		}

	}

}
