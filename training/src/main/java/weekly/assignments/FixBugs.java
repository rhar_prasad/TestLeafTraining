package weekly.assignments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class FixBugs {
	@Test
	// method signature is wrong --added by Hari
	public void main() throws InterruptedException {

		// launch the browser

		// chrome drive key is wrong. --added by Hari
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.get("https://www.myntra.com/");

		// Added code to maximize the window --added by Hari
		driver.manage().window().maximize();

		// Mouse Over on Men
		Actions builder = new Actions(driver);

		// link text is wrong.Changed from Men to MEN --added by Hari
		builder.moveToElement(driver.findElementByLinkText("MEN")).perform();
		// Added wait for 1/2 seconds to have sync
		Thread.sleep(5000);
		// Click on Jackets
		driver.findElementByXPath("//a[@href='/men-jackets']").click();

		// Find the count of Jackets

		// following-sibling is wrong --added by Hari
		String leftCount = driver.findElementByXPath("//input[@value='Jackets']/following-sibling::span").getText()
				.replaceAll("//D", "");
		System.out.println(leftCount);

		// Click on Jackets and confirm the count is same
		driver.findElementByXPath("//label[text()='Jackets']").click();

		// Wait for some time
		Thread.sleep(5000);

		// Check the count
		// following-sibling is wrong and text also wrong --added by Hari
		String rightCount = driver.findElementByXPath("//h1[text()='Jackets for Men']/following-sibling::span")
				.getText().replaceAll("//D", "");
		System.out.println(rightCount);

		// If both count matches, say success
		if (leftCount.equals(rightCount)) {
			System.out.println("The count matches on either case");
		} else {
			System.err.println("The count does not match");
		}

		// Click on Offers
		// text is wrong. It should be brand name --added by Hari
		driver.findElementByXPath("//h3[text()='Nike']/following::a").click();

		// Find the costliest Jacket
		List<WebElement> productsPrice = driver.findElementsByXPath("//span[@class='product-discountedPrice']");
		List<String> onlyPrice = new ArrayList<String>();

		for (WebElement productPrice : productsPrice) {
			onlyPrice.add(productPrice.getText().replaceAll("//D", ""));
		}

		// Sort them
		String max = Collections.max(onlyPrice);

		// Find the top one
		System.out.println(max);

		// commented below line as it will close the current browser --added by
		// Hari
		// driver.close();

		// Print Only Allen Solly Brand Minimum Price
		// following-sibling syntax is wrong --added by Hari
		// Allen Solly is not in the top 10 list. So we need to add the step to
		// expand --added by Hari
		WebElement brandMore = driver.findElementByXPath("//div[@class='brand-more']");
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", brandMore);

		// following sibling syntax error --added by Hari
		driver.findElementByXPath("//input[@value='Allen Solly']/following-sibling::div").click();

		// added a step to close the expansion ----added by Hari
		driver.findElementByXPath("//span[@class='myntraweb-sprite FilterDirectory-close sprites-remove']").click();
		// Find the cheapest Jacket
		List<WebElement> allenSollyPrices = driver.findElementsByXPath("//span[@class='product-discountedPrice']");

		// declared new List for allen solly --added by Hari
		List<String> allenPriceList = new ArrayList<String>();

		for (WebElement allenPrices : allenSollyPrices) {
			allenPriceList.add(allenPrices.getText().replaceAll("//D", ""));
		}

		// Get the minimum Price
		String min = Collections.min(allenPriceList);

		// Find the lowest priced Allen Solly
		System.out.println(min);

	}
}
