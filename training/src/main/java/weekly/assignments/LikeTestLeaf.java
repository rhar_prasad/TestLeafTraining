package weekly.assignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

public class LikeTestLeaf {

	public static void main(String[] args) throws InterruptedException {
		// Launch Browser with disable notification
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeOptions ops = new ChromeOptions();
		ops.addArguments("-disable-notifications");
		ChromeDriver driver = new ChromeDriver(ops);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Launch facebook and login
		driver.get("https://www.facebook.com/");
		driver.findElementById("email").sendKeys("8884151543");
		driver.findElementById("pass").sendKeys("India682");
		driver.findElementByXPath("//input[@value='Log In']").click();

		// Search TestLeaf
		driver.findElementByXPath("//input[@data-testid='search_input']").sendKeys("TestLeaf", Keys.ENTER);
		// driver.findElementByXPath("//input[@data-testid='facebar_search_button']").click();

		Thread.sleep(2000);

		Actions builder = new Actions(driver);

		WebElement places = driver.findElementByXPath("(//div[text()='Places'])[2]");

		builder.moveToElement(places).click(places).perform();

		Thread.sleep(3000);

		WebElement testLeaf = driver.findElementByXPath("//a[text()='TestLeaf']");

		//Verify the TestLeaf link under places
		if (testLeaf.isDisplayed()) {
			//Verify the like button text
			String likeText = driver.findElementByXPath("(//button[@data-testid='search_like_button_test_id'])[1]")
					.getText();
			if (likeText.contains("Like")) {
				driver.findElementByXPath("(//button[@data-testid='search_like_button_test_id'])[1]").click();
			} else {
				System.out.println("Already Liked");
			}
			
			//Navigate to the TestLeaf page
			driver.findElementByXPath("//a[text()='TestLeaf']").click();

			String title = driver.getTitle();
			//Verify the browser title for TestLeaf
			System.out.println(title);
			if (title.contains("TestLeaf")) {
				System.out.println("Browser title contains TestLeaf");
			} else {
				System.out.println("Browser title does not contain TestLeaf");
			}

			String numPeople = driver.findElementByXPath("//img[@alt='Highlights info row image']/following::div/div")
					.getText();

			String peopleLiked = numPeople.replaceAll("\\D", "");
			System.out.println(peopleLiked);

		} else {
			System.out.println("TestLeaf is not displayed under places");
		}

		driver.close();

	}

}
