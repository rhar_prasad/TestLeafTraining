package weekly.assignments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class SecondHighest {

	public static void main(String[] args) {
		Map<Character,Integer> charMap=new LinkedHashMap<Character,Integer>();
		String str="PayPal India";
		
		char[] charArray = str.toCharArray();
		
		for (char ch : charArray) {
			
			if(charMap.containsKey(ch)){
				charMap.put(ch, charMap.get(ch)+1);
			}
			else{
				charMap.put(ch, 1);
			}
			
		}
		
		List<Integer> valList=new ArrayList<Integer>(charMap.values());
				
		Collections.sort(valList);
		Collections.reverse(valList);
				
		Set<Entry<Character, Integer>> entrySet = charMap.entrySet();
		
		for (Entry<Character, Integer> entry : entrySet) {
			
			if(entry.getValue()==valList.get(1)){
				System.out.println(entry.getKey());
			}
			
		}

	}

}
