package weekly.assignments;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWebtable {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/table.html");

		// to print the webtable header
		List<WebElement> tHeader = driver.findElementsByXPath("//table//tr[1]/th");
		int colCount = tHeader.size();
		for (WebElement tableHeader : tHeader) {

			System.out.println(tableHeader.getText());

		}

		// to print the table data

		List<WebElement> tRows = driver.findElementsByXPath("//table//tr");
		int rowCount = tRows.size();

		for (int i = 2; i <= rowCount; i++) {

			for (int j = 1; j < colCount; j++) {
				String colData = driver.findElementByXPath("//table//tr[" + i + "]/td[" + j + "]").getText();
				System.out.println(colData);
			}

		}

	}

}
