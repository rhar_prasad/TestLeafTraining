package weekly.assignments;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HighPriceCar {
	public static int greatestPrice(int[] price, int length) {
		int temp;
		for (int i = 0; i < length; i++) {
			for (int j = i + 1; j < length; j++) {
				if (price[i] > price[j]) {
					temp = price[i];
					price[i] = price[j];
					price[j] = temp;

				}

			}

		}
		return price[length - 1];

	}

	public static void main(String[] args) {
		String carInfo;
		String carModel = "";
		String txtPrice = "";

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://www.zoomcar.com/chennai/");
		driver.findElementByLinkText("Start your wonderful journey").click();
		driver.findElementByXPath("(//div[@class='items'])[2]").click();
		driver.findElementByXPath("//button[@class='proceed']").click();
		driver.findElementByXPath("//button[@class='proceed']").click();
		driver.findElementByXPath("//button[@class='proceed']").click();

		// To get all the price
		List<WebElement> allPrice = driver.findElementsByXPath("//div[@class='price']");
		for (WebElement price : allPrice) {
			txtPrice = txtPrice + price.getText();
		}
		// to remove "₹"
		String[] split = txtPrice.split("₹ ");

		// converting the price string to number array
		int[] numPrice = new int[split.length];
		for (int i = 1; i < split.length; i++) {
			numPrice[i - 1] = Integer.parseInt(split[i]);

		}

		// to find highest number
		int maxPrice = greatestPrice(numPrice, numPrice.length);

		// Converting max price to string
		String highestPrice = Integer.toString(maxPrice);

		// To get the model of the highest price
		List<WebElement> availableCars = driver.findElementsByName("book-now");

		int size = availableCars.size();
		for (int i = 1; i <= size; i++) {
			carInfo = driver.findElementByXPath("(//div[@class='car-item'])[" + i + "]").getText();
			if (carInfo.contains(highestPrice)) {
				carModel = driver.findElementByXPath("(//div[@class='car-item'])[" + i + "]/div/h3").getText();
				break;
			}
		}

		System.out.println("Car with highest price: " + carModel);

	}

}
