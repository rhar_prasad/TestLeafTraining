package week5.day2;

import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LearnExcel {
	
	public static String[][] readExcel(String filename) throws InvalidFormatException, IOException {
		
		XSSFWorkbook wb=new XSSFWorkbook(new File("./data/"+filename+".xlsx"));
		XSSFSheet ws = wb.getSheetAt(0);
		//XSSFSheet sheet = wb.getSheet("Sheet1");
		int rowCount=ws.getLastRowNum();
		int cellCount=ws.getRow(0).getLastCellNum();
		
		String[][] data=new String[rowCount][cellCount];
		
		for (int i = 1; i <=rowCount; i++) {
			XSSFRow row = ws.getRow(i);
			for (int j = 0; j < cellCount; j++) {
				data[i-1][j]=row.getCell(j).getStringCellValue();
				
			}
			
		}
		wb.close();
		
		return data;
		
		
	}

}
