package week5.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CreateLead extends Annotations{
	@BeforeTest
	public void setData(){
		dataFile="createLeadData";
	}
	
	@Test(dataProvider="getData")
	public void runCreateLead(String cname,String fname,String lname) throws InterruptedException {
		
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys(cname);
		driver.findElementById("createLeadForm_firstName").sendKeys(fname);
		driver.findElementById("createLeadForm_lastName").sendKeys(lname);
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Hari");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Krishnan");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Mr");

		WebElement source = driver.findElementById("createLeadForm_dataSourceId");
		Select ddSource = new Select(source);
		ddSource.selectByVisibleText("Website");

		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Mr");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("1000000");

		WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
		Select ddIndustry = new Select(industry);
		ddIndustry.selectByVisibleText("Computer Software");

		WebElement ownership = driver.findElementById("createLeadForm_ownershipEnumId");
		Select ddOwnership = new Select(ownership);
		ddOwnership.selectByVisibleText("Corporation");

		WebElement marketing = driver.findElementById("createLeadForm_marketingCampaignId");
		Select ddMarketing = new Select(marketing);
		ddMarketing.selectByVisibleText("Automobile");

		driver.findElementById("createLeadForm_sicCode").sendKeys("159");
		driver.findElementById("createLeadForm_description").sendKeys("Automation Testing");
		driver.findElementById("createLeadForm_importantNote").sendKeys("Test Script for practise");
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("91");

		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("044");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("8884151543");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("684");
		driver.findElementById("createLeadForm_departmentName").sendKeys("Testing");

		WebElement prefCurrency = driver.findElementById("createLeadForm_currencyUomId");
		Select ddPrefCurrency = new Select(prefCurrency);
		ddPrefCurrency.selectByVisibleText("INR - Indian Rupee");

		driver.findElementById("createLeadForm_numberEmployees").sendKeys("10");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("#");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Amma");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("www.facebook.com");
		driver.findElementById("createLeadForm_generalToName").sendKeys("Hari");

		driver.findElementById("createLeadForm_generalAddress1").sendKeys("111 Raniammai street");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("Gerugambakkam");

		WebElement country = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select ddCountry = new Select(country);
		ddCountry.selectByVisibleText("India");

		Thread.sleep(2000);
		WebElement state = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select ddState = new Select(state);
		ddState.selectByVisibleText("TAMILNADU");

		driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600128");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("128");

		driver.findElementById("createLeadForm_primaryEmail").sendKeys("r.hariprasad@gmail.com");

		driver.findElementByName("submitButton").click();

		String firstName = driver.findElementByXPath("//span[@id='viewLead_firstName_sp']").getText();

		if (firstName.equals("Hari Prasad")) {
			System.out.println("Lead created successfully");
		} else {
			System.err.println("Lead not created successfully");
		}

	}
	
	

}
