package week5.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class DuplicateLead extends Annotations {
	@Test
	public void runDuplicateLead(String[] args) {
	
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Hari Prasad");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		WebDriverWait wait = new WebDriverWait(driver, 10);

		String leadId = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a")))
				.getText();

		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();

		driver.findElementByLinkText("Duplicate Lead").click();

		String winTitle = driver.getTitle();

		if (winTitle.contains("Duplicate Lead")) {
			System.out.println("Duplicate Lead page is opened up");
		} else {
			System.out.println("Duplicate Lead page is not opened up");
		}

		driver.findElementByName("submitButton").click();

		String leadName = driver.findElementById("viewLead_firstName_sp").getText();

		if (leadName.equals("Hari Prasad")) {
			System.out.println("Duplicate lead created successfully");
		} else {
			System.out.println("Duplicate not lead created successfully");
		}
	}

}
