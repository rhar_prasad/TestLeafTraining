package week5.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class DeleteLead extends Annotations {
	@Test
	public void runDeleteLead() {
		
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();

		driver.findElementByXPath("//span[text()='Phone']").click();

		driver.findElementByName("phoneNumber").sendKeys("8884151543");

		driver.findElementByXPath("//button[text()='Find Leads']").click();

		WebDriverWait wait = new WebDriverWait(driver, 10);

		String leadId = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a")))
				.getText();

		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();

		driver.findElementByLinkText("Delete").click();

		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByName("id").sendKeys(leadId);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		String errTxt = driver.findElementByXPath("//div[text()='No records to display']").getText();

		if (errTxt.equals("No records to display")) {
			System.out.println("Record deleted successfully");
		} else {
			System.out.println("Record not deleted successfully");
		}

	}

}
