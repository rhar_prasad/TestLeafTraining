package week5.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class EditLead extends Annotations {
	@BeforeTest
	public void setData(){
		dataFile="editleadData";
	}
	
	@Test(dataProvider="getData")
	public void runEditLead(String fname,String cname) {
		
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys(fname);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		WebDriverWait wait = new WebDriverWait(driver, 10);

		String leadId = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a")))
				.getText();

		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();
		driver.findElementByLinkText("Edit").click();
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys(cname);
		driver.findElementByName("submitButton").click();

		String compName = driver.findElementById("viewLead_companyName_sp").getText();

		if (compName.contains("Revive")) {
			System.out.println("Company name update successfully");
		} else {
			System.out.println("Company name not updated successfully");
		}

	}

}
