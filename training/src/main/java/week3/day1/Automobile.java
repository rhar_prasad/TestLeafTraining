package week3.day1;

public interface Automobile {
	public void accidentAlarm();
	public void enableQuietMode();
	public void autoAirFill();

}
