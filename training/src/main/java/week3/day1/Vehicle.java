package week3.day1;

public class Vehicle {
	
	public void accelerate() {
		System.out.println("Accelerate the vehicle");

	}
	
	public void soundHorn() {
		System.out.println("Applying normal horn");

	}
	
	public void soundHorn(String airHorn) {
		System.out.println("Applying normal horn");

	}
	
	public void applyBrake() {
		System.out.println(" applying brake");

	}
	
	
	

}
