package week3.day1;

public class HighEndVehicle extends AutoVehicle {

	public static void main(String[] args) {
		HighEndVehicle hev=new HighEndVehicle();
		
		hev.accidentAlarm();
		hev.autoAirFill();
		hev.enableQuietMode();

	}

	public void accidentAlarm() {
		System.out.println("Accident Alarm");
		
	}

	public void autoAirFill() {
		System.out.println("Air fill started ");
		
	}

}
