package week3.day2;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class CountRepeated {

	public static void main(String[] args) {
		Map<Character,Integer> charMap=new TreeMap<Character,Integer>();
		String str="Amazon India Private Limited";
		
		char[] charArray = str.toCharArray();
		
		for (char ch : charArray) {
			
			if(charMap.containsKey(ch)){
				charMap.put(ch, charMap.get(ch)+1);
			}
			else{
				charMap.put(ch, 1);
			}
			
		}
		
		Set<Character> keys = charMap.keySet();
		
		for (Character character : keys) {
			if(character!=' ')
			System.out.println(character+" -> "+charMap.get(character));
			
		}
		
		

	}

}
