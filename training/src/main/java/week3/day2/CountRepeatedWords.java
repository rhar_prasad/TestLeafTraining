package week3.day2;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CountRepeatedWords {

	public static void main(String[] args) {
		Map<String,Integer> wMap=new HashMap<String,Integer>();
		
		String text="Welcome to the java session session";
		
		String[] words = text.split(" ");
		
		for (String wList : words) {
			
			if(wMap.containsKey(wList)){
				wMap.put(wList, wMap.get(wList)+1);
			}
			else{
				wMap.put(wList, 1);
			}
			
		}
		
		Set<String> keys = wMap.keySet();
		
		for (String keysList : keys) {
			if(wMap.get(keysList)>1)
				System.out.println(keysList+" --> "+wMap.get(keysList));
			
		}
		

	}

}
