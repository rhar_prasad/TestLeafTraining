package week3.day2;

import java.util.HashSet;
import java.util.Set;

public class DuplicateCharacters {

	public static void main(String[] args) {
		
		Set<Character> charSet=new HashSet<Character>();
		String str="Infosys Limited";
		
		char[] charArray = str.toCharArray();
		
		System.out.println("Duplicate characters are: ");
		for (char ch : charArray) {
			if(!charSet.add(ch)){
				System.out.println(ch);
			}
					
		}
		
		
		
		

	}

}
