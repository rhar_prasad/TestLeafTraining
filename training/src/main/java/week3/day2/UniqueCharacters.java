package week3.day2;

import java.util.HashSet;
import java.util.Set;

public class UniqueCharacters {

	public static void main(String[] args) {
		Set<Character> charSet=new HashSet<Character>();
		String str="Amazon India Private Limited";
		
		char[] charArray = str.toCharArray();
		
		System.out.print("Unique characters are: ");
		for (char ch : charArray) {
			if(ch!=' '){
				if(charSet.add(ch)){
					System.out.print(ch+" ");
				}
				
			}
			
					
		}
		

	}

}
