package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandling {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/nget/train-search");
		
		driver.findElementByLinkText("AGENT LOGIN").click();
		
		File source = driver.getScreenshotAs(OutputType.FILE);
		File target=new File("./snaps/image1.png");
		FileUtils.copyFile(source, target);
		
		System.out.println("Before Switch :"+driver.getTitle());
		
		driver.findElementByLinkText("Contact Us").click();
		
		Set<String> allWinRef = driver.getWindowHandles();
		List<String> listRef=new ArrayList<String>(allWinRef);
		
		driver.switchTo().window(listRef.get(1));
		System.out.println("After Switch :"+driver.getTitle());
		
		driver.switchTo().window(listRef.get(0));
		driver.close();

	}

}
