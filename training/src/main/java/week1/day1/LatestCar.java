package week1.day1;

public class LatestCar {

	public static void main(String[] args) {
		CarModels cm=new CarModels();
		
		String brand="Honda";
		
		String latest=cm.models(brand);
		
		System.out.println("Latest model of "+brand+" is "+latest);
		
		

	}

}
