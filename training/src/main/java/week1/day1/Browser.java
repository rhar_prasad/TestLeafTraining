package week1.day1;

public class Browser {
	
	public String name="chrome";
	public double version=5.25;
	public String title="google";
	
	public void launchBrowser() {
		System.out.println("open browser");

	}
	
	public void closeBrowser() {
		System.out.println("close browser");

	}
	
	public void refresh() {
		System.out.println("refresh");

	}

}
