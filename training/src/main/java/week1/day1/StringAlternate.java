package week1.day1;

public class StringAlternate {

	public static void main(String[] args) {
		String txt="test";
		
		
		
		for (int i = 0; i < txt.length(); i++) {
			char ch = txt.charAt(i);
			
			if(i%2==0){
				System.out.print(Character.toLowerCase(ch));
			}else{
				System.out.print(Character.toUpperCase(ch));
			}
			
			
			
		}

	}

}
