package week1.day1;

public class CarModels {
	
	public String models(String brand) {
		
		if(brand.equals("Hyundai")){
			return "Venue 2.5";
		}else if(brand.equals("Honda")){
			return "Honda 1.5";
		}else if(brand.equals("Tata")){
			return "Tata 5.5";
		}else
			return "Brand not in the list";

		

	}

}
