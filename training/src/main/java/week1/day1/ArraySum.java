package week1.day1;

public class ArraySum {

	public static void main(String[] args) {
		int arrayOdd[]={3,10,13,25,40};
		int total=0;
		int modVal;
		
		for (int i = 0; i < arrayOdd.length; i++) {
			
			modVal=arrayOdd[i]%2;
			
			if(modVal!=0){
				total=total+arrayOdd[i];
			}
			
			
		}
		
		System.out.println("Sum of odd numbers in the array is: "+total);

	}

}
