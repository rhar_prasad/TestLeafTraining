Feature: Lead functionality in the leaf taps application

Background:
Given open the chrome browser
And maximize the Browser
And set the timeouts
And load the url
And login using username as DemoSalesManager
And enter password as crmsfa
And click the login button
And click on crmsfa link
And click on Leads tab

Scenario Outline: TC001 Create lead with company name, first name and last name

And click on CreateLead link
And enter company name as <company>
And enter firstname as <firstname>
And enter lastname as <lastname>
When click on submit button
Then lead should be created

Examples:
|company|firstname|lastname|
|TestLeaf|Hari|Radhakrishnan|
|Fidelity|Prasad|Radhakrishnan|


Scenario Outline: TC002 Edit lead for the given firstnames

Given click on find leads
And enter name as <firstName>
And click find leads button
And wait for the result to be loaded
And click the first result
And click on edit button
And clear the existing value
And enter the new company name as <company>
When click on submit
Then new company name should be displayed as <company>

Examples:
|firstName|company|
|Hari|Fidelity|
|Prasad|Revive|

Scenario Outline: TC003 Delete lead for the given phone number

Given click on find leads
And click on phone tab
And enter phone number as <phone>
And click find leads button
And click the first result
When click on delete button
And click on find leads
And enter lead id
And click find leads button
Then the record should be deleted

Examples:
|phone|
|8884151543|
|99|