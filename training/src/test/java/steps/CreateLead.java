package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {
	public ChromeDriver driver;
	@Given("open the chrome browser")
	public void openTheChromeBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Given("maximize the Browser")
	public void maximizeTheBrowser() {
		driver.manage().window().maximize();
	}

	@Given("set the timeouts")
	public void setTheTimeouts() {
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	@Given("load the url")
	public void loadTheUrl() {
		driver.get("http://leaftaps.com/opentaps/control/main");
	}

	@Given("login using username as (.*)")
	public void loginUsingUsernameAsDemoSalesManager(String uname) {
		driver.findElementById("username").sendKeys(uname);
	}

	@Given("enter password as (.*)")
	public void enterPasswordAsCrmsfa(String pwd) {
		driver.findElementById("password").sendKeys(pwd);
	}

	@Given("click the login button")
	public void clickTheLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
	}
	
	@Given("click on crmsfa link")
	public void clickOnCrmsfaLink() {
		driver.findElementByLinkText("CRM/SFA").click();
	}
	@Given("click on Leads tab")
	public void clickOnLeadsTab() {
		driver.findElementByLinkText("Leads").click();
	}
	@Given("click on CreateLead link")
	public void clickOncreateLeadLink() {
		driver.findElementByLinkText("Create Lead").click();
	}
	

	@Given("enter company name as (.*)")
	public void enterCompanyNameAsTestLeaf(String cname) {
		driver.findElementById("createLeadForm_companyName").sendKeys(cname);
	}

	@Given("enter firstname as (.*)")
	public void enterFirstnameAsHari(String fname) {
		driver.findElementById("createLeadForm_firstName").sendKeys(fname);
	}

	@Given("enter lastname as (.*)")
	public void enterLastnameAsRadhakrishnan(String lname) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lname);
	}

	@When("click on submit button")
	public void clickOnSubmitButton() {
		driver.findElementByName("submitButton").click();
	}

	@Then("lead should be created")
	public void leadShouldBeCreated() {
		
		System.out.println("created successfully");
		String firstName = driver.findElementByXPath("//span[@id='viewLead_firstName_sp']").getText();

		if (firstName.equals("Hari")) {
			System.out.println("Lead created successfully");
		} else {
			System.err.println("Lead not created successfully");
		}
	}
	
	
	//Edit leads methods
	@Given("click on find leads")
	public void clickOnFindLeads() throws InterruptedException {
		driver.findElementByLinkText("Find Leads").click();
	}
	@Given("enter name as (.*)")
	public void enterNameAs(String fname) {
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys(fname);
	}
	@Given("click find leads button")
	public void clickFindLeadsButton() {
		driver.findElementByXPath("//button[text()='Find Leads']").click();
	}

	@Given("wait for the result to be loaded")
	public void waitForTheResultToBeLoaded() {
		WebDriverWait wait = new WebDriverWait(driver, 10);

		String leadId = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a")))
				.getText();
	}

	@Given("click the first result")
	public void clickTheFirstResult() {
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();
	}

	@Given("click on edit button")
	public void clickOnEditButton() {
		driver.findElementByLinkText("Edit").click();
	}

	@Given("clear the existing value")
	public void clearTheExistingValue(){
	driver.findElementById("updateLeadForm_companyName").clear();
	}

	@Given("enter the new company name as (.*)")
	public void enterTheNewCompanyNameAsFidelity(String comp) {
		driver.findElementById("updateLeadForm_companyName").sendKeys(comp);
	}

	@When("click on submit")
	public void clickOnSubmit() {
		driver.findElementByName("submitButton").click();
	}

	@Then("new company name should be displayed as (.*)")
	public void newCompanyNameShouldBeDisplayed(String comp) {
String compName = driver.findElementById("viewLead_companyName_sp").getText();
		
		if(compName.contains(comp)){
			System.out.println("Company name update successfully");
		}
		else{
			System.out.println("Company name not updated successfully");
		}
	}

}
